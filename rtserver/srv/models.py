from django.db import models
from channels import Group
import json
from .settings import MSG_TYPE_MESSAGE
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.db.models import signals
from phonenumber_field.modelfields import PhoneNumberField


class Room(models.Model):
    """
    Room model for contract consumers
    """
    title = models.CharField(
        max_length=15,
        verbose_name='Заголовок'
    )
    user = models.ManyToManyField(User, verbose_name='Пользователи')

    @property
    def websocket_group(self):
        """
        This property defines a group, which defined for this Room
        :return: Group instance
        """
        return Group("room-%s" % self.id)


    def send_message(self, message, user, msg_type=MSG_TYPE_MESSAGE):
        """
        Called to send a message to the room on behalf of a user.
        :param message: Message
        :param user: User instance
        :param msg_type: message type. By default MSG_TYPE_MESSAGE
        :return: void
        """

        final_msg = {
            'room': str(self.id),
            'message': message,
            'username': user.username,
            'msg_type': msg_type
        }

        # Send out the message to everyone in the room
        self.websocket_group.send(
            {"text": json.dumps(final_msg)}
        )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Комната'
        verbose_name_plural = 'Комнаты'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = PhoneNumberField(default='', verbose_name='Номер телефона')
    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Device(models.Model):
    title = models.CharField(max_length=15)
    status = models.CharField(max_length=15)

    def __str__(self):
        return self.title


class Equipment(models.Model):
    device_id = models.ForeignKey(Device)
    title = models.CharField(max_length=15)
    indicator = models.CharField(max_length=15)
    trigger = models.BooleanField(default=False)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class Command():
    def __init__(self, room, type_message):
        self.room = room
        self.type_message = type_message

    # Событие от периферийного датчика
    def input_message_event(self, device, equipment, event, data, time_command, cause):
        pass

    # Ответ на запрос о передаче текущей конфигурации
    def input_message_config(self, device, equipment, event, data, time_command, cause):
        pass


    # Запрос общего состояния
    def input_message_request_status(self, device):
        pass


    # Запрос конфигурации устройства
    def input_message_request_config(self, device):
        user_output_config(self, device)


    # Запрос на изменение конфигурации устройства
    def input_message_change_config(self, device, equipment, data):
        pass


    # Команда на осуществление управляющего воздействия
    def input_message_control_action(self, device, equipment, data):
        device_output_control_action(self, device, equipment, data)


    # Команда на изменение конфигурации и осуществление управляющего воздействия
    def send_message_device_command(self, device, equipment, data):
        final_msg = {
            'room': str(self.room),
            'device': str(device),
            'equipment': str(equipment),
            'data': str(data)
        }
        self.room.websocket_group.send({"text": json.dumps(final_msg)})


    # Ответ о приеме и постановке в очередь
    def send_message_queue(self, device, data, user):
        final_msg = {
            'room': str(self.room),
            'username': user.username,
            'device': str(device),
            'data': str(data)
        }
        self.room.websocket_group.send({"text": json.dumps(final_msg)})


    # Отправка текущей конфигурации
    def send_message_config(self, device, equipment, event, data, time_command, cause):
        final_msg = {
            'room': str(self.room),
            'device': str(device),
            'equipment': str(equipment),
            "event": event,
            "data": str(data),
            "time_command": str(time_command),
            "cause": cause,
        }
        self.room.websocket_group.send({"text": json.dumps(final_msg)})


    # Передача общего состояния системы
    def send_message_status(self, data):
        final_msg = {
            'room': str(self.room),
            'data': str(data),
        }
        self.room.websocket_group.send({"text": json.dumps(final_msg)})


    def send_message_emergency_event(self, device, equipment, data):
        final_msg = {
            'room': str(self.room),
            'device': str(device),
            'data': str(data)
        }
        self.room.websocket_group.send({"text": json.dumps(final_msg)})
