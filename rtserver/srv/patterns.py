# from .consumers import srv_message2
#
#
# class Test():
#     """
#     Receiver
#     """
#     def message(self, message: str):
#         srv_message1(message)
#
#     def stop(self, message: str):
#         srv_message2(message)
#
# class Command:
#     """
#     Базовый класс для всех команд
#     """
#     def execute(self):
#         raise NotImplementedError()
#
#     def unexecute(self):
#         raise NotImplementedError()
#
#
# class Test1Command(Command):
#     """
#     Команда1
#     """
#     def __init__(self, test: Test):
#         self.test = test
#
#     def execute(self):
#         self.test.message("execute Test1")
#
#     def unexecute(self):
#         self.test.stop("unexecute Test1")
#
#
# class Test2Command(Command):
#     """
#     Команда2
#     """
#     def __init__(self, test: Test):
#         self.test = test
#
#     def execute(self):
#         self.test.message("execute Test2")
#
#     def unexecute(self):
#         self.test.stop("unexecute Test2")
#
#
# class TestInterface:
#     """
#     Invoker
#     """
#     def __init__(self, test1: Test1Command, test2: Test2Command):
#         self.test1_command = test1
#         self.test2_command = test2
#         self.current_command = None        # команда, выполняющаяся в данный момент
#
#     def test1(self):
#         self.current_command = self.test1_command
#         self.test1_command.execute()
#
#     def test2(self):
#         self.current_command = self.test2_command
#         self.test2_command.execute()
#
#     def stop(self):
#         if self.current_command:
#             self.current_command.unexecute()
#             self.current_command = None
#         else:
#             print('Невозможно выполнить команду!')
