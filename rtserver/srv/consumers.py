import json, sys, datetime
from channels import Channel
from channels.auth import channel_session_user_from_http, channel_session_user, http_session_user

from .settings import NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS, MSG_TYPE_ENTER, MSG_TYPE_LEAVE
from .models import Room, Device, Equipment, Command
from .utils import catch_client_error, get_room_or_error, get_room, get_device, get_equipment
from .exceptions import ClientError

from inspect import getmembers




# This decorator copies the user from the HTTP session (only available in
# websocket.connect or http.request messages) to the channel session (available
# in all consumers with the same reply_channel, so all three here)
@channel_session_user_from_http
def ws_connect(message):
    # pprint(getmembers(message))
    message.reply_channel.send({"accept": True})
    message.channel_session['rooms'] = []


@channel_session_user
def ws_disconnect(message):
    # Unsubscribe from any connected rooms
    print('Выход из комнаты')

    for room_id in message.channel_session.get("rooms", set()):
        try:
            room = Room.objects.get(pk=room_id)
            # Removes us from the room's send group. If this doesn't get run,
            # we'll get removed once our first reply message expires.
            room.websocket_group.discard(message.reply_channel)
        except Room.DoesNotExist:
            pass


# Unpacks the JSON in the received WebSocket frame and puts it onto a channel
# of its own with a few attributes extra so we can route it
# This doesn't need @channel_session_user as the next consumer will have that,
# and we preserve message.reply_channel (which that's based on)
def ws_receive(message):
    # All WebSocket frames have either a text or binary payload; we decode the
    # text part here assuming it's JSON.
    # You could easily build up a basic framework that did this encoding/decoding
    # for you as well as handling common errors.
    print('ws.receive')
    payload = json.loads(message['text'])
    payload['reply_channel'] = message.content['reply_channel']
    Channel('srv.receive').send(payload)


# Channel_session_user loads the user out from the channel session and presents
# it as message.user. There's also a http_session_user if you want to do this on
# a low-level HTTP handler, or just channel_session if all you want is the
# message.channel_session object without the auth fetching overhead.
@channel_session_user
@catch_client_error
def srv_join(message):
    # Find the room they requested (by ID) and add ourselves to the send group
    # Note that, because of channel_session_user, we have a message.user
    # object that works just like request.user would. Security!
    print('srv_join')
    room = get_room_or_error(message["room"], message.user)

    # Send a "enter message" to the room if available
    if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
        room.send_message(None, message.user, MSG_TYPE_ENTER)


    # OK, add them in. The websocket_group is what we'll send messages
    # to so that everyone in the room gets them.
    room.websocket_group.add(message.reply_channel)
    message.channel_session['rooms'] = list(set(message.channel_session['rooms']).union([room.id]))
    # Send a message back that will prompt them to open the room
    # Done server-side so that we could, for example, make people
    # join rooms automatically.
    message.reply_channel.send({
        "text": json.dumps({
            "join": str(room.id),
            "title": room.title,
            "username": str(message.user),
        }),
    })


@channel_session_user
@catch_client_error
def srv_leave(message):
    # Reverse of join - remove them from everything.
    print('srv_leave')
    room = get_room_or_error(message["room"], message.user)

    # Send a "leave message" to the room if available
    if NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
        room.send_message(None, message.user, MSG_TYPE_LEAVE)

    room.websocket_group.discard(message.reply_channel)
    message.channel_session['rooms'] = list(set(message.channel_session['rooms']).difference([room.id]))
    # Send a message back that will prompt them to close the room
    message.reply_channel.send({
        "text": json.dumps({
            "leave": str(room.id),
        }),
    })


@channel_session_user
@catch_client_error
def srv_send(message):
    print('srv_send')
    if int(message['room']) not in message.channel_session['rooms']:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room_or_error(message["room"], message.user)
    room.send_message(message["message"], message.user)


# Событие от периферийного датчика
@channel_session_user
@catch_client_error
def device_input_event_sensor(message):
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    device = get_device(message["device"])
    equipment = get_equipment(message["equipment"])
    event = message["event"]
    data = message["data"]
    time_command = datetime.strptime(message["time_command"], "%d/%m/%y %H:%M")
    cause = message["cause"]
    command = Command(room, "input")
    command.input_message_event(device, equipment, event, data, time_command, cause)


# Ответ на запрос о передаче текущей конфигурации
@channel_session_user
@catch_client_error
def device_input_transfer_config(message):
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    device = get_device(message["device"])
    equipment = get_equipment(message["equipment"])
    event = message["event"]
    data = message["data"]
    time_command = datetime.strptime(message["time_command"], "%d/%m/%y %H:%M")
    cause = message["cause"]
    command = Command(room, "input")
    command.input_message_config(device, equipment, event, data, time_command, cause)


# Запрос общего состояния
@channel_session_user
@catch_client_error
def user_input_request_status(message):
    id_device = message["message"]["id_device"]
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    device = get_device(id_device)
    command = Command(room, "input")
    command.input_message_request_status(device)


# Запрос конфигурации устройства
@channel_session_user
@catch_client_error
def user_input_request_config(message):
    id_device = message["message"]["id_device"]
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    device = get_device(id_device)
    command = Command(room, "input")
    command.input_message_request_config(device)



# Запрос на изменение конфигурации устройства
@channel_session_user
@catch_client_error
def user_input_request_change_config(message):
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    id_device = message["message"]["id_device"]
    id_equipment = message["message"]["id_equipment"]
    data = message["message"]["data"]
    user = message.user
    device = get_device(id_device)
    equipment = get_equipment(id_equipment)
    command = Command(room, "input")
    command.input_message_change_config(device, equipment, data)
    user_output_queue(room, device, data, user)
    device_output_change_config(command, device, equipment, data)



# Команда на осуществление управляющего воздействия
@channel_session_user
@catch_client_error
def user_input_control_action(message):
    if int(message["room"]) not in message.channel_session["rooms"]:
        raise ClientError("ROOM_ACCESS_DENIED")
    room = get_room(message["room"])
    id_device = message["message"]["id_device"]
    id_equipment = message["message"]["id_equipment"]
    data = message["message"]["data"]
    device = get_device(id_device)
    equipment = get_equipment(id_equipment)
    command = Command(room, "input")
    command.input_message_control_action(device, equipment, data)


# Команда на изменение конфигурации
def device_output_change_config(command, device, equipment, data):
    command.send_message_device_command(device, equipment, data)


# Команда на осуществление управляющего воздействия
def device_output_control_action(command, device, equipment, data):
    command.send_message_device_command(device, equipment, data)


# Ответ о приеме и постановке в очередь команды от устройства
def device_output_queue(room, device, data, user):
    command = Command(room, "output")
    command.send_message_queue(device, data, user)


# Отправка текущей конфигурации
def user_output_config(command, device):
    cause = "user request"
    command.send_message_config(device, equipment, event, data, time_command, cause)


# Передача общего состояния
def user_output_status(command, device):
    command.send_message_status(data)


# Последние события
def user_output_recent_event(room, device):
    pass


# Передача показания с конкретного датчика
def user_output_indicator(room, device):
    pass


# Ответ о приеме и постановке в очередь команды от устройства
def user_output_queue(room, device, data, user):
    command = Command(room, "output")
    command.send_message_queue(device, data, user)


# Отчет об экстренном событии
def user_output_emergency_event(room, device, equipment, data):
    pass
