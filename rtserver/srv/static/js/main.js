$( document ).ready((function(){
    var ws_scheme = window.location.protocol == 'https:' ? 'wss' : 'ws';
    var ws_path = ws_scheme + '://' + window.location.host + "/srv/stream/";
    var socket = new ReconnectingWebSocket(ws_path);

    // Helpful debugging
    socket.onopen = function () {
        console.log("Connected to server socket");
    };
    socket.onclose = function () {
        console.log("Disconnected from server socket");
    };

    // Says if we joined a room or not by if there's a div for it
    inRoom = function (roomId) {
        return $("#room-" + roomId).length > 0;
    };
    // Room join/leave
    $("li.room-link").click(function () {
        roomId = $(this).attr("data-room-id");
        if (inRoom(roomId)) {
            // Leave room
            $(this).removeClass("joined");
            socket.send(JSON.stringify({
                "command": "leave",  // determines which handler will be used (see chat/routing.py)
                "room": roomId
            }));
        } else {
            // Join room
            $(this).addClass("joined");
            socket.send(JSON.stringify({
                "command": "join",
                "room": roomId
            }));
        }
    });

    socket.onmessage = function (message) {
        // Decode the JSON
        console.log("Got websocket message " + message.data);
        var data = JSON.parse(message.data);
        // Handle errors
        if (data.error) {
            alert(data.error);
            return;
        }
        // Handle joining
        if (data.join) {
            console.log("Joining room " + data.join);
            var roomdiv = $(
                "<div class='room' id='room-" + data.join + "'>" +
                "<h2>" + data.title + "</h2>" +
                "<div class='messages'></div>" +
                "<select id='command'>" +
                "<option value='team_selection'>Выберите команду</option>" +
                "<option value='system_status_query'>Запрос общего состояния системы</option>" +
                "<option value='device_config_request'>Запрос конфигурации устройства</option>" +
                "<option value='config_change_request'>Запрос на изменение конфигурации</option>" +
                "<option value='control_action'>Команда на осуществление управляющего воздействия</option>" +
                "</select>" +
                "<div id='" + data.join + "'></div>" +
                "</div>"
            );
            $("#chats").append(roomdiv);
            var data_command;
            roomdiv.find("#command").on("change",function(){
                var path = jQuery(this).val();
                id_data = "#" + data.join;
                switch (path) {
                    case "team_selection":
                        $(id_data).empty();
                        break;
                    case "system_status_query":
                        $(id_data).empty();
                        data_command = $(
                            "<br>" +
                            "<label>ID охранного устройства:</label>" +
                            "<input class='id_device'>" +
                            "<button class='send'>Отправить</button><br>"
                        );
                        $(id_data).append(data_command);
                        break;
                    case "device_config_request":
                        $(id_data).empty();
                        data_command = $(
                            "<br>" +
                            "<label>ID охранного устройства:</label>" +
                            "<input class='id_device'>" +
                            "<button class='send'>Отправить</button><br>"
                        );
                        $(id_data).append(data_command);
                        break;
                    case "config_change_request":
                        $(id_data).empty();
                        data_command = $(
                            "<br>" +
                            "<label>ID охранного устройства:</label>" +
                            "<input class='id_device'>" +
                            "<br>" +
                            "<label>ID периферийного устройства:</label>" +
                            "<input class='id_equipment'>" +
                            "<br>" +
                            "<label>Данные:</label>" +
                            "<input class='data'>" +
                            "<br>" +
                            "<button class='send'>Отправить</button><br>"
                        );
                        $(id_data).append(data_command);
                        break;
                    case "control_action":
                        $(id_data).empty();
                        data_command = $(
                            "<br>" +
                            "<label>ID охранного устройства:</label>" +
                            "<input class='id_device'>" +
                            "<br>" +
                            "<label>ID периферийного устройства:</label>" +
                            "<input class='id_equipment'>" +
                            "<br>" +
                            "<label>Данные:</label>" +
                            "<input class='data'>" +
                            "<br>" +
                            "<button class='send'>Отправить</button><br>"
                        );
                        $(id_data).append(data_command);
                        break;
                }
                $('button.send').click(function(){
                    switch (path) {
                        case "system_status_query":
                            var message = {
                                "id_device": $('input.id_device').val()
                            }
                            socket.send(JSON.stringify({
                                "command": "system_status_query",
                                "room": data.join,
                                "message": message
                            }));
                            $('input.id_device').val("");
                            break;
                        case "device_config_request":
                            var message = {
                                "id_device": $('input.id_device').val()
                            }
                            socket.send(JSON.stringify({
                                "command": "device_config_request",
                                "room": data.join,
                                "message": message
                            }));
                            $('input.id_device').val("");
                            break;
                        case "config_change_request":
                            var message = {
                                "id_device": $('input.id_device').val(),
                                "id_equipment": $('input.id_equipment').val(),
                                "data": $('input.data').val(),
                            }
                            socket.send(JSON.stringify({
                                "command": "config_change_request",
                                "room": data.join,
                                "message": message
                            }));
                            $('input.id_device').val("");
                            $('input.id_equipment').val("");
                            $('input.data').val("");
                            break;
                        case "control_action":
                            var message = {
                                "id_device": $('input.id_device').val(),
                                "id_equipment": $('input.id_equipment').val(),
                                "data": $('input.data').val(),
                            }
                            socket.send(JSON.stringify({
                                "command": "control_action",
                                "room": data.join,
                                "message": message
                            }));
                            $('input.id_device').val("");
                            $('input.id_equipment').val("");
                            $('input.data').val("");
                            break;
                    }
                });
            });
        // Handle leaving
        } else if (data.leave) {
                console.log("Leaving room " + data.leave);
                $("#room-" + data.leave).remove();
        }
        else if (data.message || data.msg_type != 0) {
            var msgdiv = $("#room-" + data.room + " .messages");
            console.log(data.username)
            var ok_msg = "";
            // msg types are defined in srv/settings.py
            // Only for demo purposes is hardcoded, in production scenarios, consider call a service.
            switch (data.msg_type) {
                case 0:
                    // Message
                    ok_msg = "<div class='message'>" +
                        "<span class='username'>" + data.username + "</span>" +
                        "<span class='roomid'>" + data.room + "</span>" +
                        "<span class='body'>" + data.message + "</span>" +
                        "</div>";
                    break;
                case 1:
                    // Warning/Advice messages
                    ok_msg = "<div class='contextual-message text-warning'>" + data.message + "</div>";
                    break;
                case 2:
                    // Alert/Danger messages
                    ok_msg = "<div class='contextual-message text-danger'>" + data.message + "</div>";
                    break;
                case 3:
                    // "Muted" messages
                    ok_msg = "<div class='contextual-message text-muted'>" + data.message + "</div>";
                    break;
                case 4:
                    // User joined room
                    ok_msg = "<div class='contextual-message text-muted'>" + data.username + " joined the room!" + "</div>";
                    break;
                case 5:
                    // User left room
                    ok_msg = "<div class='contextual-message text-muted'>" + data.username + " left the room!" + "</div>";
                    break;
                default:
                    console.log("Unsupported message type!");
                    break;
            }
            msgdiv.append(ok_msg);
            msgdiv.scrollTop(msgdiv.prop("scrollHeight"));
        } else {
            console.log("Cannot handle message!");
        }

    }
  })
);
