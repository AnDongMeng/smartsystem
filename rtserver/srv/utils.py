from functools import wraps
from .exceptions import ClientError
from django.contrib.auth.models import User
from .models import Room, Device, Equipment


def catch_client_error(func):
    """
    Decorator to catch the ClientError exception and translate it into a reply.
    :param func: function, which is decorated
    :return: function inner or exception
    """
    print('error')
    @wraps(func)
    def inner(message, *args, **kwargs):
        try:
            return func(message, *args, **kwargs)
        except ClientError as e:
            # If we catch a client error, tell it to send an error string
            # back to the client on their reply channel
            e.send_to(message.reply_channel)
    return inner


def get_room_or_error(room_id, user):
    """
    Tries to fetch a room for the user, checking permissions along the way.
    :param room_id: Room ID
    :param user: User instance
    :return: Room instance or exception
    """
    # Check if the user is logged in
    if not user.is_authenticated():
        raise ClientError("USER_HAS_TO_LOGIN")

    # Find the room by ID
    try:
        room = Room.objects.get(pk=room_id)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")

    users = User.objects.filter(room=room)
    check = False
    for u in users:
        if u == user:
            check = True

    # Check permissions
    if not check and not user.is_staff:
        raise ClientError("ROOM_ACCESS_DENIED")

    return room


def get_room(room_id):
    # Find the room by ID
    try:
        room = Room.objects.get(pk=room_id)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")

    return room


def get_device(device_id):
    try:
        device = Device.objects.get(pk=device_id)
    except Device.DoesNotExist:
        raise ClientError("DEVICE_INVALID")

    return device


def get_equipment(equipment_id):
    try:
        equipment = Equipment.objects.get(pk=equipment_id)
    except Equipment.DoesNotExist:
        raise ClientError("EQUIPMENT_INVALID")

    return equipment
