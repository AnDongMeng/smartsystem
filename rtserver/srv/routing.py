from channels import route
from .consumers import ws_connect, ws_receive, ws_disconnect
from .consumers import srv_join, srv_leave, srv_send
from .consumers import user_input_request_status
from .consumers import user_input_request_config
from .consumers import user_input_request_change_config
from .consumers import user_input_control_action


# There's no path matching on these routes; we just rely on the matching
# from the top-level routing. We could path match here if we wanted.
websocket_routing = [
    # Called when WebSockets connect
    route("websocket.connect", ws_connect),

    # Called when WebSockets get sent a data frame
    route("websocket.receive", ws_receive),

    # Called when WebSockets disconnect
    route("websocket.disconnect", ws_disconnect),
]



# You can have as many lists here as you like, and choose any name.
# Just refer to the individual names in the include() function.
custom_routing = [
    # Handling different commands (websocket.receive is decoded and put
    # onto this channel) - routed on the "command" attribute of the decoded
    # message.
    route("srv.receive", srv_join, command="^join$"),
    route("srv.receive", srv_leave, command="^leave$"),
    route("srv.receive", srv_send, command="^send$"),
    route("srv.receive", user_input_request_status, command="^system_status_query$"),
    route("srv.receive", user_input_request_config, command="^device_config_request$"),
    route("srv.receive", user_input_request_change_config, command="^config_change_request$"),
    route("srv.receive", user_input_control_action, command="^control_action$"),
]
