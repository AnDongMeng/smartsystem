from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Room

@login_required
def index(request):
    """
    Root page view.
    :param request: incoming request
    :return: HTTP-object
    """
    user = request.user
    if user.is_staff:
        rooms = Room.objects.order_by('title')
    else:
        rooms = Room.objects.filter(user=user)
    return render(request, "index.html", {
        'rooms': rooms,
    })
