from django.contrib import admin
from .models import Room, Profile, Device, Equipment
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


class RoomAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]
    list_display_links = ["id", "title"]
    search_fields = ["id", "title"]


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Профили'


# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (ProfileInline, )


class DeviceAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "status"]
    list_display_links = ["id", "title"]


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(Equipment)
